package algoritmen;

/**
 * 
 * 
 * @author Chris
 * 
 *         This Class is used to store the html tags, I only used those that are
 *         the most frequently used, easy to add others
 */

public class HtmlTagLibrary {
	/**
	 * 
	 * The StringBuffer makes it easier to reuse the tags list. You only have to
	 * modify that one list (Keep in mind that you might have to increase the length
	 * if you add many extra tags) (This is not the best way to do this)
	 * 
	 */
	private StringBuffer buffer = new StringBuffer();
	private String[] tags = new String[] { "<h1>", "<h2>", "<h3>", "<p>", "<header>", "<footer>", "<html>", "<body>",
			"<head>", "<b>", "<i>", "<li>", "<ol>", "<table>", "<tr>", "<th>", "<td>" };
	private int numberOfTags = 17;
	private String[] openTags = new String[numberOfTags];

	private String[] closingTags = new String[numberOfTags];

	/**
	 * 
	 * This is the constructor of our Library
	 * 
	 */
	public HtmlTagLibrary() {
		for (int i = 0; i < numberOfTags; i++) {
			openTags[i] = tags[i];
			buffer.append(tags[i]);
			buffer.insert(1, "/");
			String str = buffer.toString();
			closingTags[i] = str;
		}

	}

	public int getNumberOfTags() {
		return numberOfTags;
	}

	public String[] returnOpenTagsList() {
		return openTags;
	}

	public String[] returnClosingTagsList() {
		return closingTags;
	}
}
