package algoritmen;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Simple GUI for a text editor.
 *
 */
public class Texed extends JFrame implements DocumentListener {
	private JTextArea textArea;
	private StackAdapter<String> redo = new StackAdapter<String>(50);
	private StackAdapter<String> undo = new StackAdapter<String>(50);
	private HtmlTagLibrary library = new HtmlTagLibrary();
	private String invoer;
	private String outTag;
	

	/**
	 * Backspace will be used as the undo button, the right arrow to redo.
	 * 
	 */
	private static final long serialVersionUID = 5514566716849599754L;

	/**
	 * Constructs a new GUI: A TextArea on a ScrollPane
	 */
	public Texed() {
		super();
		setTitle("Texed: simple text editor");
		setBounds(800, 800, 600, 600);
		textArea = new JTextArea(30, 80);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		// Registration of the callback
		textArea.getDocument().addDocumentListener(this);

		textArea.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				
				/**
				 * 
				 * Implements the undo function
				 * 
				 */
				if (((e.getKeyCode() == KeyEvent.VK_Z) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0))) {
					
					String text = textArea.getText();
					redo.push(undo.pop());
					textArea.remove(text.length());
					

				}
				/**
				 * 
				 * Implements the redo function
				 * 
				 */
				if ((e.getKeyCode() == KeyEvent.VK_Y) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
					String str = redo.pop();
					undo.push(str);
					textArea.append(str);
	
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				char ch = e.getKeyChar();
				invoer = Character.toString(ch);
				undo.push(invoer);
				checkForClosingTags();
				addClosingTag();

			}

			@Override
			public void keyReleased(KeyEvent e) {
			
				
			}
		});

		JScrollPane scrollPane = new JScrollPane(textArea);
		add(scrollPane);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * 
	 * 
	 * @return a boolean that shows us if the backspace has been used or not
	 */

	/**
	 * Callback when changing an element
	 */
	public void changedUpdate(DocumentEvent ev) {
	}

	/**
	 * Callback when deleting an element
	 */
	public void removeUpdate(DocumentEvent ev) {
	}

	/**
	 * Callback when inserting an element
	 */
	public void insertUpdate(DocumentEvent ev) {
		// Check if the change is only a single character, otherwise return so it does
		// not go in an infinite loop
		if (ev.getLength() != 1)
			return;

		// In the callback you cannot change UI elements, you need to start a new
		// Runnable
		SwingUtilities.invokeLater(new Task(""));
	}

	/**
	 * 
	 * 
	 * Checks for a complete opening tag and will add the accompanying closing tag
	 * after checking if the tag is in our library.
	 * 
	 * 
	 */
	public void addClosingTag() {
		int cursorPos = textArea.getCaretPosition();
		String text = textArea.getText();
		if (text.lastIndexOf(">",cursorPos) == cursorPos && text.charAt(cursorPos-1)=='>') {
			int posOpenTagBeginning = text.lastIndexOf("<",cursorPos);
			int posOpenTagEnding = text.lastIndexOf(">",cursorPos);
			StringBuffer tag = new StringBuffer(text.substring(posOpenTagBeginning, posOpenTagEnding));
			String str = tag.toString();
			String[] openTags = library.returnOpenTagsList();
			String[] closingTags = library.returnClosingTagsList();
			for (int i = 0; i < library.getNumberOfTags(); i++) {

				if (openTags[i] == str) {
					outTag = closingTags[i];

				} else {
					
				}

			}
		}

		textArea.insert(outTag, cursorPos);
		
	}

	/**
	 * 
	 * This method will check if the number of ">" divided by 2 equals the number of
	 * "</". If those numbers are the same, all opening tags will have a closing tag
	 */
	public void checkForClosingTags() {
		String text = textArea.getText();
		int countClosingTags = 0;
		int countOpeningTags = 0;
		for (int i = 0; i < text.length() - 1; i++) {

			String str = new StringBuilder().append(text.charAt(i)).append(text.charAt(i + 1)).toString();
			if (str == "</") {
				countClosingTags = +1;
			} else {
			}
			String str2 = new StringBuilder().append(text.charAt(i)).toString();
			if (str2 == ">") {
				countOpeningTags = +1;
			} else {
			}
		}
		if (countClosingTags == countOpeningTags / 2) {
		} else {
			textArea.append("Not all opening tags have a closing tag");
		}
	}

	/**
	 * Runnable: change UI elements as a result of a callback Start a new Task by
	 * invoking it through SwingUtilities.invokeLater
	 */
	private class Task implements Runnable {
		private String text;

		/**
		 * Pass parameters in the Runnable constructor to pass data from the callback
		 * 
		 * @param text
		 *            which will be appended with every character
		 */
		Task(String text) {
			this.text = text;
		}

		/**
		 * The entry point of the runnable
		 */
		public void run() {
			textArea.append(text);
		}
	}

	/**
	 * Entry point of the application: starts a GUI
	 */
	public static void main(String[] args) {
		new Texed();

	}

}
