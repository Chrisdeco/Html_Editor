package algoritmen;

import java.util.EmptyStackException;

/**
 
 *
 */
public class StackAdapter<E> implements Stack<E> {
	private int length;
	private LinkedList<E> list;

	public StackAdapter(int length) {
		list = new LinkedList<E>();
		this.length = length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algoritmen.Stack#push(java.lang.Object)
	 */
	@Override
	public void push(E element) {
		list.prepend(element);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algoritmen.Stack#top()
	 */
	@Override
	public E top() {
		if (isEmpty())
			throw new EmptyStackException();
		return list.getHead();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algoritmen.Stack#pop()
	 */
	@Override
	public E pop() {
		E element = top();
		list = list.getTail();
		return element;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algoritmen.Stack#size()
	 */
	@Override
	public int size() {
		return list.getSize();
	}
		
		

	/*
	 * (non-Javadoc)
	 * 
	 * @see algoritmen.Stack#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}
	/**
	 * 
	 * This removes the first input of the Linkedlist/Stack
	 */
	public void removeFirstInput() {
	if(list.getSize()==length) {
		  list.remove(list.getHead());
	
	}
	
	

}
}
	
